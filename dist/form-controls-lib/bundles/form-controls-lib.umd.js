(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('form-controls-lib', ['exports', '@angular/core'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global['form-controls-lib'] = {}, global.ng.core));
}(this, (function (exports, i0) { 'use strict';

    var FormControlsLibService = /** @class */ (function () {
        function FormControlsLibService() {
        }
        FormControlsLibService.prototype.getData = function () {
            return 'hello world!!';
        };
        return FormControlsLibService;
    }());
    FormControlsLibService.ɵprov = i0.ɵɵdefineInjectable({ factory: function FormControlsLibService_Factory() { return new FormControlsLibService(); }, token: FormControlsLibService, providedIn: "root" });
    FormControlsLibService.decorators = [
        { type: i0.Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    FormControlsLibService.ctorParameters = function () { return []; };

    var FormControlsLibComponent = /** @class */ (function () {
        function FormControlsLibComponent() {
        }
        FormControlsLibComponent.prototype.ngOnInit = function () {
        };
        return FormControlsLibComponent;
    }());
    FormControlsLibComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'lib-form-controls-lib',
                    template: "\n    <p>\n      form-controls-lib works!\n    </p>\n  "
                },] }
    ];
    FormControlsLibComponent.ctorParameters = function () { return []; };

    var FormControlsLibModule = /** @class */ (function () {
        function FormControlsLibModule() {
        }
        return FormControlsLibModule;
    }());
    FormControlsLibModule.decorators = [
        { type: i0.NgModule, args: [{
                    declarations: [FormControlsLibComponent],
                    imports: [],
                    exports: [FormControlsLibComponent]
                },] }
    ];

    /*
     * Public API Surface of form-controls-lib
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.FormControlsLibComponent = FormControlsLibComponent;
    exports.FormControlsLibModule = FormControlsLibModule;
    exports.FormControlsLibService = FormControlsLibService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=form-controls-lib.umd.js.map
