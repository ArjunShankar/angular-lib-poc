import { ɵɵdefineInjectable, Injectable, Component, NgModule } from '@angular/core';

class FormControlsLibService {
    constructor() { }
    getData() {
        return 'hello world!!';
    }
}
FormControlsLibService.ɵprov = ɵɵdefineInjectable({ factory: function FormControlsLibService_Factory() { return new FormControlsLibService(); }, token: FormControlsLibService, providedIn: "root" });
FormControlsLibService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
FormControlsLibService.ctorParameters = () => [];

class FormControlsLibComponent {
    constructor() { }
    ngOnInit() {
    }
}
FormControlsLibComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-form-controls-lib',
                template: `
    <p>
      form-controls-lib works!
    </p>
  `
            },] }
];
FormControlsLibComponent.ctorParameters = () => [];

class FormControlsLibModule {
}
FormControlsLibModule.decorators = [
    { type: NgModule, args: [{
                declarations: [FormControlsLibComponent],
                imports: [],
                exports: [FormControlsLibComponent]
            },] }
];

/*
 * Public API Surface of form-controls-lib
 */

/**
 * Generated bundle index. Do not edit.
 */

export { FormControlsLibComponent, FormControlsLibModule, FormControlsLibService };
//# sourceMappingURL=form-controls-lib.js.map
