import { TestBed } from '@angular/core/testing';

import { FormControlsLibService } from './form-controls-lib.service';

describe('FormControlsLibService', () => {
  let service: FormControlsLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormControlsLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
