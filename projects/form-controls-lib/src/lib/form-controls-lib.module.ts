import { NgModule } from '@angular/core';
import { FormControlsLibComponent } from './form-controls-lib.component';



@NgModule({
  declarations: [FormControlsLibComponent],
  imports: [
  ],
  exports: [FormControlsLibComponent]
})
export class FormControlsLibModule { }
