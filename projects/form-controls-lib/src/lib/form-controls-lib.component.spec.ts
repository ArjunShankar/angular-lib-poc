import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormControlsLibComponent } from './form-controls-lib.component';

describe('FormControlsLibComponent', () => {
  let component: FormControlsLibComponent;
  let fixture: ComponentFixture<FormControlsLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormControlsLibComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormControlsLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
