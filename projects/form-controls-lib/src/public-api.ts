/*
 * Public API Surface of form-controls-lib
 */

export * from './lib/form-controls-lib.service';
export * from './lib/form-controls-lib.component';
export * from './lib/form-controls-lib.module';
